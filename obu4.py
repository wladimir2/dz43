# -*- coding: utf-8 -*-
import pylab
import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import export_graphviz
import graphviz
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn import metrics
from pydotplus import graph_from_dot_data


df = pd.read_csv('clean-last.txt',sep='\t')
y = df[['tag']]    # Target variable
X = df[['w', 'h']] # Features
X['wh']  = X['w'] * X['h']
X['wdh'] = X['w'] / X['h']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.3)

tree1 = DecisionTreeClassifier(criterion='gini', max_depth=7, random_state=1)
tree1.fit(X_train,y_train)
predictions = tree1.predict(X_test)
print(metrics.accuracy_score(y_test, predictions))

le = LabelEncoder()
le.fit(y)
iris_pred_names = le.classes_
dot_data = export_graphviz(tree1,
                           filled=True, 
                           rounded=True,
                           class_names=iris_pred_names,
                           feature_names=['w', 'h', 'wh', 'wdh'],
                           out_file=None) 
graph = graph_from_dot_data(dot_data)
graph.write_png('tree.png')

def predict (w, h):
 df = pd.DataFrame({'w' : [w], 'h' : [h], 'wh' : [w*h], 'wdh' : [(w+0.0)/h]})
 predictions = tree1.predict(df)
 return predictions

print(predict(800,500))


